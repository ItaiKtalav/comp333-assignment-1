package optimalBreaks;

import java.util.ArrayList;;

public class SetAndScore<T> extends ArrayList<T> {

	private static final long serialVersionUID = 1L;
	
	private int score;
	
	public SetAndScore(int score) {
		super();
		this.setScore(score);
	}
	
	public SetAndScore(ArrayList<T> arr, int score) {
		super(arr);
		this.setScore(score);
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}
	
	public void addScore(int score) {
		this.score += score;
	}
	
	public void conjoin(SetAndScore<T> other) {
		for (T i : other) {
			add(i);
		}
		this.score += other.getScore();
	}

}
