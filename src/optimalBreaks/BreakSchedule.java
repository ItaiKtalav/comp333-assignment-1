package optimalBreaks;

import java.util.ArrayList;
import java.util.HashMap;
//import java.util.*;

public class BreakSchedule {
	
	private HashMap<String, Integer> memo; 
	
	// Use this class to implement programs for Tasks 2 & 3. Your file must not change the package name,
	// nor the class name. You must not change the names nor the signatures of the two program stubs
	// in this file. You may augment this file with any other method or variable/data declarations that you
	// might need to implement the dynamic programming strategy requested for Tasks 2&3.
	// Make sure however that all your declarations are public.
	
	
	// Precondition: word is a string and breakList is an array of integers in strictly increasing order
	//               the last element of breakList is no more than the number of characters in word.
	// Postcondition: Return the minimum total cost of breaking the string word into |breakList|+1 pieces, where 
	//                the position of each each break is specified by the integers in breakList. 
	//                Refer to the assignment specification for how a single break contributes to the cost.
	
	int totalCost (String word, ArrayList<Integer> breakList) {
		memo = new HashMap<String, Integer>();
		return totalCost(word, 0, word.length()-1, breakList);
	}
	
	private int totalCost (String word, int i, int j, ArrayList<Integer> breakList) {
		
		System.out.println("\n");
		
		if (memo.containsKey(key(i, j, breakList))) {
			return memo.get(key(i, j, breakList));
		}
		
		if (j <= i) { memo.put(key(i,  j, breakList), 0); return 0; }
		if (breakList == null || breakList.isEmpty()) { memo.put(key(i,  j, breakList), 0); return 0; }
		if (breakList.size() == 1) {
			if (breakList.get(0) >= j) {
				memo.put(key(i,  j, breakList), 0); 
				return 0;
			}
			else {
				memo.put(key(i,  j, breakList), ((j - i)+1));
				return ((j - i)+1);
			}
		}
		
		
		
		
		int min = ((j - i)+1) + 
				totalCost(word, i,                  breakList.get(0),  removeItem(breakList, 0)) + 
                totalCost(word, breakList.get(0)+1, j,                 removeItem(breakList, 0));
		System.out.println(min);
		int current;
		for (int k = 1; k < breakList.size(); k++) {
			if (breakList.get(k) >= i && breakList.get(k) < j) {
				current = ((j - i)+1) + 
						totalCost(word, i,                  breakList.get(k), removeItem(breakList, k)) + 
						totalCost(word, breakList.get(k)+1, j,                removeItem(breakList, k));
			}
			else {
				current = min;
			}
			
			System.out.println(current);
			
			if (current < min){
				min = current;
			}
		}
		
		memo.put(key(i,  j, breakList), min);
		return min;
	}
	
	private String key (int i, int j, ArrayList<Integer> b) {
		return "Key [i=" + i + ", j=" + j + ", b=" + b + "]";
	}
	
	private ArrayList<Integer> removeItem(ArrayList<Integer> arr, int item) {
		ArrayList<Integer> rem = new ArrayList<Integer>(arr);
		rem.remove(item);
		return rem;
	}
	 
	// Precondition: word is a string and breakList is an array of integers in strictly increasing order
	//               the last element of breakList is no more than the number of characters in word.
	// Postcondition: Return the schedule of breaks so that word is broken according to the list of
	// 					breaks specified in breakList which yields the minimum total cost.
	 
	 ArrayList<Integer> breakSchedule (String word, ArrayList<Integer> breakList) { 
		memo = new HashMap<String, Integer>();
	
		return  (ArrayList<Integer>)breakSchedule(word, 0, word.length()-1, breakList, new SetAndScore<Integer>(Integer.MAX_VALUE), new SetAndScore<Integer>(Integer.MAX_VALUE));
	 }
	 
	 private SetAndScore<Integer> breakSchedule (String word, int i, int j, ArrayList<Integer> breakList, SetAndScore<Integer> current, SetAndScore<Integer> best) {
		
		 if (memo.containsKey(key(i, j, breakList))) {
			 if (memo.get(key(i, j, breakList)) < best.getScore()) {
					return current;
				}
				else {
					return best;
				}	
			}
			
		if (j <= i) { memo.put(key(i,  j, current), 0); return current; }
		if (breakList == null || breakList.isEmpty()) { memo.put(key(i,  j, current), 0); return current; }
		 
		if (breakList.size() == 1) {
			
			current.add(breakList.get(0));
			
			if (breakList.get(0) == i || breakList.get(0) == j) {
				memo.put(key(i,  j, current), 0); 
				
				if (current.getScore() < best.getScore()) {
					return current;
				}
				else {
					return best;
				}
			}
			else {
				
				current.addScore((j+1) - i);
				
				if (current.getScore() < best.getScore()) {
					memo.put(key(i,  j, current), (j+1) - i);
					return current;
				}
				else {
					return best;
				}
			}
		}
		
		
		
		
		
		
		SetAndScore<Integer> min = new SetAndScore<Integer>((j+1) - i);  
		current.add(breakList.get(0));
		min.conjoin(breakSchedule(word, i,                  breakList.get(0),  removeItem(breakList, 0), current, best));
		min.conjoin(breakSchedule(word, breakList.get(0)+1, j,                 removeItem(breakList, 0), current, best));
		
		SetAndScore<Integer> now;
		
		for (int k = 0; k < breakList.size(); k++) {
			if (breakList.get(k) > i && breakList.get(k) < j) {
				
				now = new SetAndScore<Integer>(j- i);  
				current.add(breakList.get(k));
				now.conjoin(breakSchedule(word, i,                  breakList.get(0),  removeItem(breakList, k), current, best));
				now.conjoin(breakSchedule(word, breakList.get(0)+1, j,                 removeItem(breakList, k), current, best));
				
				
			}
			else {
				now = min;
			}
			
			if (now.getScore() < min.getScore()){
				min = now;
			}
		}
		 
		if (best.getScore() < min.getScore()) return best;
		else return min;
	 }	 
	 
	 
	 
	 
	 
	 
	/*public static void main(String[] args) {
		ArrayList<Integer> a = new ArrayList<Integer>();
		ArrayList<Integer> b = new ArrayList<Integer>();
		for (int i=0; i < 100; i++) {
			a.add(i);
			b.add(i);
		}
		System.out.println(a.equals(b));
						 
		Key k1 = new Key(0, 6, a);
		Key k2 = new Key(0, 6, b);
		System.out.println(k1.equals(k2));
		
		HashMap<Key, Integer> memo = new HashMap<Key, Integer>();
		memo.put(k1, 12);
		System.out.println(memo.containsKey(k2));
		
		System.out.println(k1.toString().equals(k2.toString()));
	}*/
	 
	 
	 
	 
	 
	 
}
